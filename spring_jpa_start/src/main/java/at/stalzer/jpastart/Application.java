package at.stalzer.jpastart;

import org.aspectj.apache.bcel.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application implements CommandLineRunner{
	@Autowired
	private CityRepository repos;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        
    }

	@Override
	public void run(String... arg0) throws Exception {
		for(City c : repos.findAll())
			System.out.println(c);
		
	}
}
